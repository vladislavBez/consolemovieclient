﻿using System;
using System.Runtime.Serialization;
using System.Globalization;
using System.Collections.Generic;

namespace ConsoleMovieClient
{
    [DataContract]
    public class Movies
    {
        [DataMember(Name = "movies")]
        public List<Movie> MoviesList { get; set; }

    }

    [DataContract]
    public class Movie
    {
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        [DataMember(Name = "Year")]
        public string Year { get; set; }

    }
}
