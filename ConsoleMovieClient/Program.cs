﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;

namespace ConsoleMovieClient
{
    class Program
    {
        private static async Task<Movies> ProcessMovies()
        {
            var client = new HttpClient();
            //var serializer = new DataContractJsonSerializer(typeof(List<Movie>));
            var serializer = new DataContractJsonSerializer(typeof(Movies));

            client.DefaultRequestHeaders.Accept.Clear();

            var streamTask = client.GetStreamAsync("http://localhost:8080/movies");
            var movies = serializer.ReadObject(await streamTask) as Movies;

            return movies;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("The program started");
            Console.WriteLine();

            var movies = ProcessMovies().Result;

            foreach (var m in movies.MoviesList)
            {
                Console.WriteLine("{0}{1}{2}", m.Name, " - ", m.Year);

            }
            Console.WriteLine();
            Console.Write("The program is completed. Press Enter to exit");
            Console.Read();
        }
    }
}
